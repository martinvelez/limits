#limits -- checks C++ data type limits
- - -

## Table of Contents
1. Introduction
2. Dependencies
3. Installation
4. Usage
5. Development

- - -

## 1. Introduction 

[limits](https://bitbucket.org/martinvelez/limits) is a
command-line program which prints the data type limits in C++

## 2. Dependencies

### General

* [g++](http://gcc.gnu.org)


## 3. Installation

### Linux (Tested on Ubuntu 12.04)

	$ make

## 4. Usage

## Linux

	$ ./limits

## 5. Development
* Author: 					[Martin Velez](http://www.martinvelez.com)
* Copyright: 				Copyright (C) 2012 [Martin Velez](http://www.martinvelez.com)
* License: 					[GPL](http://www.gnu.org/copyleft/gpl.html)

### Source 
[Bitbucket](https://bitbucket.org/martinvelez/limits/src) is hosting this code.
	
	https://bitbucket.org/martinvelez/limits/src

### Issues
Provide feedback, get help, request features, and report bugs here:

	https://bitbucket.org/martinvelez/limits/issues

