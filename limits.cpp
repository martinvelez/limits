#include <iostream>
#include <limits>

using namespace std;

int main()
{
	cout << boolalpha;
	cout << "INT_MIN = " << numeric_limits<int>::min() << endl;	
	cout << "INT_MAX = " << numeric_limits<int>::max() << endl;
	cout << "int is signed: " << numeric_limits<int>::is_signed << endl;
	cout << "Non-sign bits in int: " << numeric_limits<int>::digits << endl;
	cout << endl;
	cout << "SHRT_MIN = " << numeric_limits<short int>::min() << endl;
	cout << "SHRT_MAX = " << numeric_limits<short int>::max() << endl;
	cout << "short int is signed: " << numeric_limits<short int>::is_signed << endl;
	cout << "Non-sign bits in short int: " << numeric_limits<short int>::digits << endl;
	cout << endl;
	cout << "LONG_MIN = " << numeric_limits<long int>::min() << endl;
	cout << "LONG_MAX = " << numeric_limits<long int>::max() << endl;
	cout << "long int is signed: " << numeric_limits<long int>::is_signed << endl;
	cout << "Non-sign bits in long int: " << numeric_limits<long int>::digits << endl;
	cout << endl;
	cout << "LONG_LONG_MIN = " << numeric_limits<long long int>::min() << endl;
	cout << "LONG_LONG_MAX = " << numeric_limits<long long int>::max() << endl;
	cout << "long long int is signed: " << numeric_limits<long long int>::is_signed << endl;
	cout << "Non-sign bits in long long int: " << numeric_limits<long long int>::digits << endl;
	cout << endl;
	cout << "CHAR_MIN = " << static_cast<short int>(numeric_limits<char>::min()) << endl;
	cout << "CHAR_MAX = " << static_cast<short int>(numeric_limits<char>::max()) << endl;
	cout << "char is signed: " << numeric_limits<char>::is_signed << endl;
	cout << "Non-sign bits in char: " << numeric_limits<char>::digits << endl;
	return 0;
}
