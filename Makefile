.PHONY: all clean

CPPFLAGS+=-c -Wall
all: limits

clean:
	-rm -f *.o limits core

limits: limits.cpp
	g++ limits.cpp -o limits
